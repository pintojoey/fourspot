ISD PROJECT REPORT
NARIYAL ANNA
						26/04/2013

Joey Pinto          2013KUCP1021
Megha Gupta    2013KUCP1027
                                     
INTRODUCTION
‘Nariyal Anna’ is a two player AI based ping-pong style game for the android platform designed keeping in mind parameters of friction and motion responsiveness.
Technology
Android
Java
Motion Sensors for android
Smart Algorithm for Computer Player
Variable game speeds
Specifications:
Number of players : 2
Computer controlled players: 1
User controlled players: 1
Game Max Score: 21

Design
The game displays two paddles on either side of the screen, with a circular ball bouncing between them. The play area on the screen is separated from the rest of the display by lines at the top and bottom. There is  a scoreboard which displays the current score of both users.

Game Objective
To make the coconut touch the bottom of the opponent’s area.
To reach a score of 21 first
Game Search Space
1.Ball direction 2 integers monitoring deltaX and deltaY
2.Ball position  X and Y Coordinates
3.Paddle Position X Coordinates for each
Algorithm
	GamePlay
The X-axis and Y-axis Position of the ball is constantly being monitored.
We are monitoring a horizontal position value of both paddles normalized to be a value between -100 and 100. (‘0’ being the center).
We are monitoring ball moment direction in variables ΔX and ΔY five the slope of the moment of the ball
Gameplay states:
Ball in mid space:
Detected when ball’s Y position is greater than player_paddle_height but lesser than (screen_height-paddle_height).
No Action is taken
b. Ball touching computer paddle
Detected when ball’s Y position is equal to  paddle_height and X position is equal to the computer_paddle_position.
We check the moving direction of the paddle and also the ball.
If movement of ball and paddle is in same direction ΔX is increased in angle of tanθ 
Else we decrease θ
c. Ball touching computer ground
Detected when ball’s Y position is equal to  computer_paddle_height and X position is not equal and far from the computer_paddle_position.
Score is incremented for user
d.Ball touching user paddle
Detected when ball’s Y position is equal to  screen_height - paddle_height and X position is equal to the user_paddle_position.
We check the moving direction of the paddle and also the ball.
If movement of ball and paddle is in same direction ΔX then we increase the angle of tanθ 
Else we decrease θ
                 e.   Ball touching user ground
            ix. Detected when ball’s Y position is equal to  user_paddle_height and X                                                      position is not equal and far from the user_paddle_position.
                x.. Score is incremented for computer.



AI USED-
The AI algorithm used above lets the user feel he/she is playing against a real person as the algorithm thinks in a way similar to users do. Also the Computer often looses.
Basic thinking strategy:
When the ball is further than 3/4th of the screen we dont really care about the ball and as far as possible try to keep our paddle at the center for best case scenario.
When the ball is closer than 3/4th of the screen we try to follow the path the ball is going and try to adapt.
When the ball is closer than 2/5th of the screen we try desperately to align the ball to the paddle.
*The following algorithm has been implemented in the function moveCompPaddle();

CONCLUSION
The game has thus implemented a reasonably accurate and complex AI algorithm in a fun to learn game.
-Happy Gaming!