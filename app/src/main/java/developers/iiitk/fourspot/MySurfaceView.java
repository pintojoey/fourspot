package developers.iiitk.fourspot;

import android.content.res.Resources;
import android.content.res.Resources;
import android.graphics.Paint;

import android.graphics.PorterDuff;
import android.graphics.PorterDuff;
import android.view.SurfaceView;

/**
 * Created by Joey and Megha Gupta on 4/10/2016.
 */
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.SurfaceHolder;


public class MySurfaceView extends SurfaceView {
    static int orientation;
    float width;
    float height;
    Paint paint;

    private SurfaceHolder surfaceHolder;
    private Bitmap coconut,beach,background;
    public static MyThread myThread;
    public static Context context;
    float xPos = 0;
    float yPos = 0;
    float deltaX = 13;
    float deltaY = 16;
    float iconWidth;
    float iconHeight;
    int topscore=21;
    float paddleWidth = 10;
    float paddleHeight = 20;
    float position_user = 0;
    float position_comp = 0;
    boolean exit = false;
    boolean user_hit=false;
    boolean comp_hit=false;
    int score_user=0;
    int score_comp=0;

    public MySurfaceView(Context context) {
        super(context);
        init();
    }

    public MySurfaceView(Context context,
                         AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MySurfaceView(Context context,
                         AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {

        myThread = new MyThread(this,this.getContext());

        surfaceHolder = getHolder();
        coconut = BitmapFactory.decodeResource(getResources(),
                R.drawable.ball);
        beach = BitmapFactory.decodeResource(getResources(),
                R.drawable.coconut);
xPos=300;
        yPos=200;

        iconWidth = coconut.getWidth();
        iconHeight = coconut.getHeight();
         beach = BitmapFactory.decodeResource(this.getResources(), R.drawable.coconut);


        surfaceHolder.addCallback(new SurfaceHolder.Callback() {

            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                myThread.setRunning(true);
                myThread.start();
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder,
                                       int format, int width, int height) {
                // TODO Auto-generated method stub

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                boolean retry = true;
                myThread.setRunning(false);
                while (retry) {
                    try {
                        myThread.join();
                        retry = false;
                    } catch (InterruptedException e) {
                    }
                }
            }
        });
    }

    protected void drawSomething(Canvas canvas) {
       beach=beach.createScaledBitmap(beach,getMeasuredWidth(),getMeasuredHeight(),false);
      canvas.drawBitmap(beach,0,0,null);
        width = getWidth();
        height = getHeight();
        moveUserPaddle();
       moveCompPaddle();
        //position=orientation*2;

        paddleHeight = 20;
        paddleWidth = width / 5;

        float factor = (getWidth() - 2 * paddleWidth) / 100;
        float start_user = getWidth() / 2 - paddleWidth / 2 + position_user * factor;
        float start_comp = getWidth() / 2 - paddleWidth / 2 + position_comp * factor;

        float end_user = getWidth() / 2 + paddleWidth / 2 + position_user * factor;
        float end_comp = getWidth() / 2 + paddleWidth / 2 + position_comp * factor;


        if ((getHeight() - iconHeight - paddleHeight) - yPos <= iconWidth
                && (xPos > start_user - iconWidth / 3 && xPos < end_user + iconWidth / 3)) { //ball hits user paddle
            user_hit=true;
        }
        else if (yPos > getHeight() - iconHeight) { //ball touches ground
         score_comp++;

//            Home.setGameScore();
        }

        else if(yPos<=0 && (xPos+iconWidth/2<start_comp-5||xPos>end_comp)){//ball touches computer ground
            score_user++;

        }
        else if(yPos<=0){//ball hits computer paddle

            comp_hit=true;
        }
        else ;
//        canvas.drawBitmap(beach,width,height,null);


        moveBall();

        canvas.drawBitmap(coconut, xPos, yPos, null);//draw ball

        paint = new Paint();//paddle color

        paint.setColor(Color.argb(990,165,42,42));
        paint.setStrokeWidth(3);


        canvas.drawRect(start_user, height - paddleHeight, end_user, height, paint);
        canvas.drawRect(start_comp, 0, end_comp, paddleHeight, paint);

        paint.setColor(Color.WHITE);
        paint.setTextSize(40);

        canvas.drawText("You:"+score_user+" Nariyal Anna:"+score_comp, width/3, height*7/8, paint);
        if(score_comp==topscore&&score_user!=topscore){canvas.drawText("You Loose! Nariyal Anna Wins", width/6, height*7/8+40, paint);exit=true;}
        if(score_comp!=topscore&&score_user==topscore){canvas.drawText("You WIN!", width/3, height*7/8+40, paint);exit=true;
        canvas.drawText("Free coconut water for you!", width/5, height*7/8+80, paint);exit=true;}
     if(exit)   myThread.setRunning(false);

    }
    private float getPosition(float xPos){
        return xPos/getWidth()*200-100;
    }
    private void moveCompPaddle() {
        if(yPos<getHeight()*2/5){
             if(getPosition(xPos)<position_comp && position_comp >-65&&deltaX<0){
                 position_comp-=3;
                 if(comp_hit==true){
                     if(deltaX<0){deltaX-=0.15;}
                     else{deltaX+=0.15;}
                     comp_hit=false;
                 }
            }
            else if(getPosition(xPos)<position_comp && position_comp <65 &&deltaX>0){
                 if(yPos<getHeight()*3/4) position_comp+=2;
                 else  position_comp+=1;
                 if(comp_hit==true){
                     if(deltaX<0){deltaX+=0.05;}
                     else{deltaX+=0.05;}
                     comp_hit=false;
                 }
            }
            else if(getPosition(xPos)>position_comp && position_comp < 65&&deltaX<0){
               position_comp+=2;
                 if(comp_hit==true){
                     if(deltaX<0){deltaX+=0.1;}
                     else{deltaX+=0.1;}
                     comp_hit=false;
                 }
             }
            else if (getPosition(xPos)>position_comp && position_comp < 65&& deltaX>0){
                position_comp+=3;
                 if(comp_hit==true){
                     if(deltaX<0){deltaX+=0.15;}
                     else{deltaX+=0.15;}
                     comp_hit=false;
                 }
            }
            else;
        }
       else  if (yPos < getHeight() * 3/4) {
            if (deltaX > 0 && position_comp < 75) {
                position_comp+= 5;
            }
            if (deltaX < 0 && position_comp > -75) {
                position_comp -= 5;
            }
        }
       else {
         //   if(position_comp>0)position_comp-=3;
           // if(position_comp<0)position_comp+=3;
        }
    }

    private void moveUserPaddle() {

        if (orientation > 8 && position_user < 75) {
            position_user += 6;
            if(user_hit==true){
                if(deltaX<0){deltaX+=0.2;}
                    else{deltaX-=0.2;}
                user_hit=false;
            }
        }
        if (orientation < -8 && position_user > -75) {
            position_user -= 6;
            if(user_hit==true){
                if(deltaX<0){deltaX-=0.2;}
                else{deltaX+=0.2;}
                user_hit=false;
            }
        }
    }

    public void moveBall() {
        xPos += deltaX;
        if (deltaX > 0) {
            if (xPos >= getWidth() - iconWidth) {
                deltaX *= -1;
            }
        } else {
            if (xPos <= 0) {
                deltaX *= -1;
            }
        }

        yPos += deltaY;
        if (deltaY > 0) {
            if (yPos >= getHeight() - iconHeight) {
                deltaY *= -1;
            }
        } else {
            if (yPos <= 0) {
                deltaY *= -1;
            }
        }
    }

}