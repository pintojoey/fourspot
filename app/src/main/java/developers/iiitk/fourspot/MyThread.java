package developers.iiitk.fourspot;

/**
 * Created by Joey on 4/10/2016.
 */


        import android.content.Context;
        import android.content.res.Resources;
        import android.graphics.Bitmap;
        import android.graphics.BitmapFactory;
        import android.graphics.Canvas;

public class MyThread extends Thread {

    MySurfaceView myView;
    private boolean running = false,ready=false;
  private Context context;

    public MyThread(MySurfaceView view,Context context) {
        myView = view;
        this.context=context;
    }

    public void setRunning(boolean run) {
        running = run;
    }

    @Override
    public void run() {
        while(running){

            Canvas canvas = myView.getHolder().lockCanvas();

            if(canvas != null){

                synchronized (myView.getHolder()) {
                    myView.drawSomething(canvas);

                }
                myView.getHolder().unlockCanvasAndPost(canvas);
            }


            try {
               Thread.sleep(10);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}